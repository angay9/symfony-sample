<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;

class PersonService 
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function person()
    {
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getManager();
        $user = $em->getRepository('AppBundle:Person')->findOneByUsername(get_current_user());

        return $user;
    }
}