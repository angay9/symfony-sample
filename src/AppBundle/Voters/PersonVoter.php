<?php

namespace AppBundle\Voters;

use AppBundle\Entity\Person;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PersonVoter extends Voter
{
    // these strings are just invented: you can use anything
    const CREATE = 'create';
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';
    const INDEX = 'index';

    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::VIEW, self::EDIT, self::DELETE, self::CREATE, self::INDEX))) {
            return false;
        }

        if (!$subject instanceof Person) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $personService = $this->container->get('app.person_service');
        $user = $personService->person();

        if (!$user) {
            return false;
        }

        switch($attribute) {
            case self::DELETE:
                return $user->isAdmin() || $subject->getUsername() == get_current_user();
            case self::EDIT:
                return $user->isAdmin() || $subject->getUsername() == get_current_user();
        }


        return $user->isAdmin()/* ? true : $person->getUsername() == get_current_user()*/;
    }

}