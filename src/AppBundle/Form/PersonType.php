<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;


class PersonType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $update = $options['data']->getId();

        $builder
            ->add('firstname', null, [
                'constraints'   =>  [
                    new Assert\NotBlank()
                ]
            ])
            ->add('lastname', null, [
                'constraints'   =>  [
                    new Assert\NotBlank()
                ]
            ])
        ;
        if (!$options['data']->isAdmin() && !$update) {
            $builder->add('username', null, [
                'constraints'   =>  [
                    new Assert\NotBlank()
                ]
            ]);
        }
        
        $builder->add('company', null, [
                'constraints'   =>  [
                    new Assert\NotBlank()
                ]
            ])
            ->add('email', null, [
                'constraints'   =>  [
                    new Assert\NotBlank(),
                    new Assert\Email()
                ]
            ])
            ->add('tel', null, [
                'constraints'   =>  [
                    new Assert\NotBlank()
                ]
            ])
            ->add('address', null, [
                'constraints'   =>  [
                    new Assert\NotBlank()
                ]
            ])
            ->add('postal', null, [
                'constraints'   =>  [
                    new Assert\NotBlank()
                ]
            ])
            ->add('city', null, [
                'constraints'   =>  [
                    new Assert\NotBlank()
                ]
            ])
            ->add('country', CountryType::class, [
                'constraints'   =>  [
                    new Assert\NotBlank(),
                    new Assert\Country()
                ]
            ])
            ->add('iban', null, [
                'constraints'   =>  [
                    new Assert\NotBlank(),
                    new Assert\Iban()
                ]
            ])
        ;
        if ($options['current_user'] && $options['current_user']->isAdmin()) {
            $builder->add('admin');
        }
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Person',
            'current_user'  =>  null
        ));
    }
}
