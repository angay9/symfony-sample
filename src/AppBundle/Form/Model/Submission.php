<?php

namespace AppBundle\Form\Model;

class Submission 
{
    /**
     * @Assert\Valid
     */
    public $submission;

    public $attachment1;

    public $attachment2;
    
    public $attachment3;
    
    public $attachment4;
    
    public $attachment5;
    
}