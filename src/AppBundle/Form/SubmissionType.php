<?php

namespace AppBundle\Form;

use AppBundle\Entity\Person;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;


class SubmissionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $person = $options['person'];

        $builder
            // ->add('reference', null, [
            //     'constraints'   =>  [
            //         new Assert\NotBlank
            //     ]
            // ])
            ->add('purpose', null, [
                'constraints'   =>  [
                    new Assert\NotBlank
                ]
            ])
            ->add('amount', null, [
                'constraints'   =>  [
                    new Assert\NotBlank
                ]
            ])
            ->add('costCenter', ChoiceType::class, [
                'choices'   =>  array_combine($options['costCenters'], $options['costCenters'])
            ])
            ->add('caseName', ChoiceType::class, [
                'choices'   =>  array_combine($options['cases'], $options['cases']),
                'constraints'   =>  [
                    new Assert\NotNull
                ]
            ])

            ->add('participantInternal1', null, [
                'constraints'   =>  [
                    new Assert\Callback([$this, 'validateParticipantInternal'])
                ]
            ])
            ->add('participantInternal2')
            ->add('participantInternal3')
            ->add('participantInternal4')
            ->add('participantInternal5')

            ->add('participantExternal1', null, [
                'constraints'   =>  [
                    new Assert\Callback([$this, 'validateParticipantExternal'])
                ]
            ])
            ->add('participantExternal2')
            ->add('participantExternal3')
            ->add('participantExternal4')
            ->add('participantExternal5')
            
            ->add('attachment1File', FileType::class, [
                'required'  =>  false,
                'constraints'   =>  [
                    new Assert\File([
                        'maxSize'   =>  '4096k',
                        'mimeTypes' =>  [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage'   =>  'Please upload a valid PDF'
                    ])
                ]
            ])
            ->add('attachment2File', FileType::class, [
                'required'  =>  false,
                'constraints'   =>  [
                    new Assert\File([
                        'maxSize'   =>  '4096k',
                        'mimeTypes' =>  [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage'   =>  'Please upload a valid PDF'
                    ])
                ]
            ])
            ->add('attachment3File', FileType::class, [
                'required'  =>  false,
                'constraints'   =>  [
                    new Assert\File([
                        'maxSize'   =>  '4096k',
                        'mimeTypes' =>  [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage'   =>  'Please upload a valid PDF'
                    ])
                ]
            ])
            ->add('attachment4File', FileType::class, [
                'required'  =>  false,
                'constraints'   =>  [
                    new Assert\File([
                        'maxSize'   =>  '4096k',
                        'mimeTypes' =>  [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage'   =>  'Please upload a valid PDF'
                    ])
                ]
            ])
            ->add('attachment5File', FileType::class, [
                'required'  =>  false,
                'constraints'   =>  [
                    new Assert\File([
                        'maxSize'   =>  '4096k',
                        'mimeTypes' =>  [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage'   =>  'Please upload a valid PDF'
                    ])
                ]
            ])
            ->add('submit', SubmitType::class, [
                'attr'  =>  [
                    'class' =>  'btn btn-primary'
                ]
            ])
        ;

    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Submission',
            'person'    =>  new Person(),
            'cases' =>  [],
            'costCenters' => [],
            'em'    =>  null
        ));
    }

    public function validateParticipantInternal($data, $context)
    {
        $form = $context->getRoot();
        $model = $form->getData();
        
        $em = $form->getConfig()->getOption('em');
        $caseName = $model->getCaseName();

        $caseEntity = $em->getRepository('AppBundle:CaseEntity')->findOneByName($caseName);
        $participantIntNeeded = $caseEntity->getParticipantInternalNeeded();
        
        if ($participantIntNeeded && !$model->getParticipantInternal1()) {
            $context->buildViolation('This case requires that you provide at least one participant internal.')
                            ->atPath('participantInternal1')
                            ->addViolation();
        }
    }

    public function validateParticipantExternal($data, $context)
    {
        $form = $context->getRoot();
        $model = $form->getData();
        
        $em = $form->getConfig()->getOption('em');
        $caseName = $model->getCaseName();

        $caseEntity = $em->getRepository('AppBundle:CaseEntity')->findOneByName($caseName);
        $participantExtNeeded = $caseEntity->getParticipantExternalNeeded();
        
        if ($participantExtNeeded && !$model->getParticipantExternal1()) {
            $context->buildViolation('This case requires that you provide at least one participant external.')
                            ->atPath('participantExternal1')
                            ->addViolation()
                        ;
        }
    }
}
