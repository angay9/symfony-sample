<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Submission
 *
 * @ORM\Table(name="submission")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubmissionRepository")
 */
class Submission
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, unique=true)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="purpose", type="string", length=255)
     */
    private $purpose;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

    /**
     * @ORM\Column(name="cost_center", type="integer")
     */
    private $costCenter;

    /**
     * @var string
     *
     * @ORM\Column(name="case_name", type="string", length=255)
     */
    private $caseName;

    /**
     * @var string
     *
     * @ORM\Column(name="participant_internal_1", type="string", length=255, nullable=true)
     */
    private $participantInternal1;

    /**
     * @var string
     *
     * @ORM\Column(name="participant_internal_2", type="string", length=255, nullable=true)
     */
    private $participantInternal2;

    /**
     * @var string
     *
     * @ORM\Column(name="participant_internal_3", type="string", length=255, nullable=true)
     */
    private $participantInternal3;

    /**
     * @var string
     *
     * @ORM\Column(name="participant_internal_4", type="string", length=255, nullable=true)
     */
    private $participantInternal4;

    /**
     * @var string
     *
     * @ORM\Column(name="participant_internal_5", type="string", length=255, nullable=true)
     */
    private $participantInternal5;

    /**
     * @var string
     *
     * @ORM\Column(name="participant_external_1", type="string", length=255, nullable=true)
     */
    private $participantExternal1;

    /**
     * @var string
     *
     * @ORM\Column(name="participant_external_2", type="string", length=255, nullable=true)
     */
    private $participantExternal2;

    /**
     * @var string
     *
     * @ORM\Column(name="participant_external_3", type="string", length=255, nullable=true)
     */
    private $participantExternal3;

    /**
     * @var string
     *
     * @ORM\Column(name="participant_external_4", type="string", length=255, nullable=true)
     */
    private $participantExternal4;

    /**
     * @var string
     *
     * @ORM\Column(name="participant_external_5", type="string", length=255, nullable=true)
     */
    private $participantExternal5;

    /**
     * @var string
     *
     * @ORM\Column(name="attachment_1", type="string", length=255, nullable=true)
     */
    private $attachment1;

    /**
     * @var string
     *
     * @ORM\Column(name="attachment_2", type="string", length=255, nullable=true)
     */
    private $attachment2;

    /**
     * @var string
     *
     * @ORM\Column(name="attachment_3", type="string", length=255, nullable=true)
     */
    private $attachment3;

    /**
     * @var string
     *
     * @ORM\Column(name="attachment_4", type="string", length=255, nullable=true)
     */
    private $attachment4;

    /**
     * @var string
     *
     * @ORM\Column(name="attachment_5", type="string", length=255, nullable=true)
     */
    private $attachment5;

    /**
     * @var string
     *
     * @ORM\Column(name="person_id", type="integer")
     */
    private $personId;

    /**
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="submissions")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     */
    private $person;

    private $attachment1File;
    private $attachment2File;
    private $attachment3File;
    private $attachment4File;
    private $attachment5File;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Submission
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set purpose
     *
     * @param string $purpose
     *
     * @return Submission
     */
    public function setPurpose($purpose)
    {
        $this->purpose = $purpose;

        return $this;
    }

    /**
     * Get purpose
     *
     * @return string
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return Submission
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set costCenter
     *
     * @param integer $costCenter
     *
     * @return Submission
     */
    public function setCostCenter($costCenter)
    {
        $this->costCenter = $costCenter;

        return $this;
    }

    /**
     * Get costCenterId
     *
     * @return int
     */
    public function getCostCenter()
    {
        return $this->costCenter;
    }

    /**
     * Set caseName
     *
     * @param string $caseName
     *
     * @return Submission
     */
    public function setCaseName($caseName)
    {
        $this->caseName = $caseName;

        return $this;
    }

    /**
     * Get caseName
     *
     * @return string
     */
    public function getCaseName()
    {
        return $this->caseName;
    }

    /**
     * Set participantInternal1
     *
     * @param string $participantInternal1
     *
     * @return Submission
     */
    public function setParticipantInternal1($participantInternal1)
    {
        $this->participantInternal1 = $participantInternal1;

        return $this;
    }

    /**
     * Get participantInternal1
     *
     * @return string
     */
    public function getParticipantInternal1()
    {
        return $this->participantInternal1;
    }

    /**
     * Set participantInternal2
     *
     * @param string $participantInternal2
     *
     * @return Submission
     */
    public function setParticipantInternal2($participantInternal2)
    {
        $this->participantInternal2 = $participantInternal2;

        return $this;
    }

    /**
     * Get participantInternal2
     *
     * @return string
     */
    public function getParticipantInternal2()
    {
        return $this->participantInternal2;
    }

    /**
     * Set participantInternal3
     *
     * @param string $participantInternal3
     *
     * @return Submission
     */
    public function setParticipantInternal3($participantInternal3)
    {
        $this->participantInternal3 = $participantInternal3;

        return $this;
    }

    /**
     * Get participantInternal3
     *
     * @return string
     */
    public function getParticipantInternal3()
    {
        return $this->participantInternal3;
    }

    /**
     * Set participantInternal4
     *
     * @param string $participantInternal4
     *
     * @return Submission
     */
    public function setParticipantInternal4($participantInternal4)
    {
        $this->participantInternal4 = $participantInternal4;

        return $this;
    }

    /**
     * Get participantInternal4
     *
     * @return string
     */
    public function getParticipantInternal4()
    {
        return $this->participantInternal4;
    }

    /**
     * Set participantInternal5
     *
     * @param string $participantInternal5
     *
     * @return Submission
     */
    public function setParticipantInternal5($participantInternal5)
    {
        $this->participantInternal5 = $participantInternal5;

        return $this;
    }

    /**
     * Get participantInternal5
     *
     * @return string
     */
    public function getParticipantInternal5()
    {
        return $this->participantInternal5;
    }

    /**
     * Set participantExternal1
     *
     * @param string $participantExternal1
     *
     * @return Submission
     */
    public function setParticipantExternal1($participantExternal1)
    {
        $this->participantExternal1 = $participantExternal1;

        return $this;
    }

    /**
     * Get participantExternal1
     *
     * @return string
     */
    public function getParticipantExternal1()
    {
        return $this->participantExternal1;
    }

    /**
     * Set participantExternal2
     *
     * @param string $participantExternal2
     *
     * @return Submission
     */
    public function setParticipantExternal2($participantExternal2)
    {
        $this->participantExternal2 = $participantExternal2;

        return $this;
    }

    /**
     * Get participantExternal2
     *
     * @return string
     */
    public function getParticipantExternal2()
    {
        return $this->participantExternal2;
    }

    /**
     * Set participantExternal3
     *
     * @param string $participantExternal3
     *
     * @return Submission
     */
    public function setParticipantExternal3($participantExternal3)
    {
        $this->participantExternal3 = $participantExternal3;

        return $this;
    }

    /**
     * Get participantExternal3
     *
     * @return string
     */
    public function getParticipantExternal3()
    {
        return $this->participantExternal3;
    }

    /**
     * Set participantExternal4
     *
     * @param string $participantExternal4
     *
     * @return Submission
     */
    public function setParticipantExternal4($participantExternal4)
    {
        $this->participantExternal4 = $participantExternal4;

        return $this;
    }

    /**
     * Get participantExternal4
     *
     * @return string
     */
    public function getParticipantExternal4()
    {
        return $this->participantExternal4;
    }

    /**
     * Set participantExternal5
     *
     * @param string $participantExternal5
     *
     * @return Submission
     */
    public function setParticipantExternal5($participantExternal5)
    {
        $this->participantExternal5 = $participantExternal5;

        return $this;
    }

    /**
     * Get participantExternal5
     *
     * @return string
     */
    public function getParticipantExternal5()
    {
        return $this->participantExternal5;
    }

    /**
     * Set attachment1
     *
     * @param string $attachment1
     *
     * @return Submission
     */
    public function setAttachment1($attachment1)
    {
        $this->attachment1 = $attachment1;

        return $this;
    }

    /**
     * Get attachment1
     *
     * @return string
     */
    public function getAttachment1()
    {
        return $this->attachment1;
    }

    /**
     * Set attachment2
     *
     * @param string $attachment2
     *
     * @return Submission
     */
    public function setAttachment2($attachment2)
    {
        $this->attachment2 = $attachment2;

        return $this;
    }

    /**
     * Get attachment2
     *
     * @return string
     */
    public function getAttachment2()
    {
        return $this->attachment2;
    }

    /**
     * Set attachment3
     *
     * @param string $attachment3
     *
     * @return Submission
     */
    public function setAttachment3($attachment3)
    {
        $this->attachment3 = $attachment3;

        return $this;
    }

    /**
     * Get attachment3
     *
     * @return string
     */
    public function getAttachment3()
    {
        return $this->attachment3;
    }

    /**
     * Set attachment4
     *
     * @param string $attachment4
     *
     * @return Submission
     */
    public function setAttachment4($attachment4)
    {
        $this->attachment4 = $attachment4;

        return $this;
    }

    /**
     * Get attachment4
     *
     * @return string
     */
    public function getAttachment4()
    {
        return $this->attachment4;
    }

    /**
     * Set attachment5
     *
     * @param string $attachment5
     *
     * @return Submission
     */
    public function setAttachment5($attachment5)
    {
        $this->attachment5 = $attachment5;

        return $this;
    }

    /**
     * Get attachment5
     *
     * @return string
     */
    public function getAttachment5()
    {
        return $this->attachment5;
    }

    /**
     * Set attachment1
     *
     * @param string $attachment1
     *
     * @return Submission
     */
    public function setAttachment1File($attachment1File)
    {
        $this->attachment1File = $attachment1File;

        return $this;
    }

    /**
     * Get attachment1File
     *
     * @return string
     */
    public function getAttachment1File()
    {
        return $this->attachment1File;
    }

    /**
     * Set attachment2
     *
     * @param string $attachment2
     *
     * @return Submission
     */
    public function setAttachment2File($attachment2File)
    {
        $this->attachment2File = $attachment2File;

        return $this;
    }

    /**
     * Get attachment2
     *
     * @return string
     */
    public function getAttachment2File()
    {
        return $this->attachment2File;
    }

    /**
     * Set attachment3
     *
     * @param string $attachment3File
     *
     * @return Submission
     */
    public function setAttachment3File($attachment3File)
    {
        $this->attachment3File = $attachment3File;

        return $this;
    }

    /**
     * Get attachment3File
     *
     * @return string
     */
    public function getAttachment3File()
    {
        return $this->attachment3File;
    }

    /**
     * Set attachment4File
     *
     * @param string $attachment4File
     *
     * @return Submission
     */
    public function setAttachment4File($attachment4File)
    {
        $this->attachment4File = $attachment4File;

        return $this;
    }

    /**
     * Get attachment4File
     *
     * @return string
     */
    public function getAttachment4File()
    {
        return $this->attachment4File;
    }

    /**
     * Set attachment5File
     *
     * @param string $attachment5File
     *
     * @return Submission
     */
    public function setAttachment5File($attachment5File)
    {
        $this->attachment5File = $attachment5File;

        return $this;
    }

    /**
     * Get attachment5File
     *
     * @return string
     */
    public function getAttachment5File()
    {
        return $this->attachment5File;
    }

    public function getPersonId()
    {
        return $this->personId;
    }

    public function setPersonId($personId)
    {
        $this->personId = $personId;

        return $this;
    }

    public function getPerson()
    {
        return $this->person;
    }

    public function setPerson($person)
    {
        $this->person = $person;

        return $this;
    }
}

