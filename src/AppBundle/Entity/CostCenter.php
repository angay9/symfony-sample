<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CostCenter
 *
 * @ORM\Table(name="cost_center")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CostCenterRepository")
 */
class CostCenter
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=255)
     */
    private $company;

    /**
     * @var int
     *
     * @ORM\Column(name="costcenter", type="integer")
     */
    private $costcenter;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company
     *
     * @param string $company
     *
     * @return CaseEntity
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set costcenter
     *
     * @param integer $costcenter
     *
     * @return CaseEntity
     */
    public function setCostcenter($costcenter)
    {
        $this->costcenter = $costcenter;

        return $this;
    }

    /**
     * Get costcenter
     *
     * @return int
     */
    public function getCostcenter()
    {
        return $this->costcenter;
    }
}

