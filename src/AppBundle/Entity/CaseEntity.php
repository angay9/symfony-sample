<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CaseEntity
 *
 * @ORM\Table(name="case_entity")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CaseEntityRepository")
 */
class CaseEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=255)
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="account_number", type="string", length=255)
     */
    private $accountNumber;

    /**
     * @var bool
     *
     * @ORM\Column(name="participant_external_needed", type="boolean")
     */
    private $participantExternalNeeded;

    /**
     * @var string
     *
     * @ORM\Column(name="participant_internal_needed", type="boolean")
     */
    private $participantInternalNeeded;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company
     *
     * @param string $company
     *
     * @return CaseEntity
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CaseEntity
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set accountNumber
     *
     * @param string $accountNumber
     *
     * @return CaseEntity
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get accountNumber
     *
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * Set participantExternalNeeded
     *
     * @param boolean $participantExternalNeeded
     *
     * @return CaseEntity
     */
    public function setParticipantExternalNeeded($participantExternalNeeded)
    {
        $this->participantExternalNeeded = $participantExternalNeeded;

        return $this;
    }

    /**
     * Get participantExternalNeeded
     *
     * @return bool
     */
    public function getParticipantExternalNeeded()
    {
        return $this->participantExternalNeeded;
    }

    /**
     * Set participantInternalNeeded
     *
     * @param string $participantInternalNeeded
     *
     * @return CaseEntity
     */
    public function setParticipantInternalNeeded($participantInternalNeeded)
    {
        $this->participantInternalNeeded = $participantInternalNeeded;

        return $this;
    }

    /**
     * Get participantInternalNeeded
     *
     * @return string
     */
    public function getParticipantInternalNeeded()
    {
        return $this->participantInternalNeeded;
    }
}

