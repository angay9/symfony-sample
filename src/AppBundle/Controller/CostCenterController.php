<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\CostCenter;
use AppBundle\Form\CostCenterType;

/**
 * CaseEntity controller.
 *
 * @Route("/costcenter")
 */
class CostCenterController extends BaseController
{
    /**
     * Lists all CaseEntity entities.
     *
     * @Route("/", name="costcenter_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $this->checkPrividges();
 
        $em = $this->getDoctrine()->getManager();

        $costCenterEntities = $em->getRepository('AppBundle:CostCenter')->findAll();

        return $this->render('AppBundle:CostCenter:index.html.twig', array(
            'costCenterEntities' => $costCenterEntities,
        ));
    }

    /**
     * Creates a new CostCenter entity.
     *
     * @Route("/new", name="costcenter_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {

        $entity = new CostCenter();
        $form = $this->createForm('AppBundle\Form\CostCenterType', $entity);
        $form->handleRequest($request);

        $this->checkPrividges();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('costcenter_show', array('id' => $entity->getId()));
        }

        return $this->render('AppBundle:CostCenter:new.html.twig', array(
            'costCenter' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a CostCenterEntity entity.
     *
     * @Route("/{id}", name="costcenter_show")
     * @Method("GET")
     */
    public function showAction(CostCenter $entity)
    {
        $this->checkPrividges();

        $deleteForm = $this->createDeleteForm($entity);

        return $this->render('AppBundle:CostCenter:show.html.twig', array(
            'costCenter' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing CostCenter entity.
     *
     * @Route("/{id}/edit", name="costcenter_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, CostCenter $entity)
    {
        $deleteForm = $this->createDeleteForm($entity);
        $editForm = $this->createForm('AppBundle\Form\CostCenterType', $entity);
        $editForm->handleRequest($request);

        $this->checkPrividges();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('costcenter_edit', array('id' => $entity->getId()));
        }

        return $this->render('AppBundle:CostCenter:edit.html.twig', array(
            'costCenter' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a CaseEntity entity.
     *
     * @Route("/{id}", name="costcenter_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, CostCenter $entity)
    {
        $form = $this->createDeleteForm($entity);
        $form->handleRequest($request);

        $this->checkPrividges();
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirectToRoute('costcenter_index');
    }

    /**
     * Creates a form to delete a CostCenter entity.
     *
     * @param CostCenter $entity The CostCenter entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CostCenter $entity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('costcenter_delete', array('id' => $entity->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
