<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\CaseEntity;
use AppBundle\Form\CaseEntityType;

/**
 * CaseEntity controller.
 *
 * @Route("/caseentity")
 */
class CaseEntityController extends BaseController
{
    /**
     * Lists all CaseEntity entities.
     *
     * @Route("/", name="caseentity_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $this->checkPrividges();

        $em = $this->getDoctrine()->getManager();

        $caseEntities = $em->getRepository('AppBundle:CaseEntity')->findAll();

        return $this->render('AppBundle:CaseEntity:index.html.twig', array(
            'caseEntities' => $caseEntities,
        ));
    }

    /**
     * Creates a new CaseEntity entity.
     *
     * @Route("/new", name="caseentity_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $caseEntity = new CaseEntity();
        $form = $this->createForm('AppBundle\Form\CaseEntityType', $caseEntity);
        $form->handleRequest($request);

        $this->checkPrividges();

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($caseEntity);
            $em->flush();

            return $this->redirectToRoute('caseentity_show', array('id' => $caseEntity->getId()));
        }

        return $this->render('AppBundle:CaseEntity:new.html.twig', array(
            'caseEntity' => $caseEntity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a CaseEntity entity.
     *
     * @Route("/{id}", name="caseentity_show")
     * @Method("GET")
     */
    public function showAction(CaseEntity $caseEntity)
    {
        $this->checkPrividges();

        $deleteForm = $this->createDeleteForm($caseEntity);

        return $this->render('AppBundle:CaseEntity:show.html.twig', array(
            'caseEntity' => $caseEntity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing CaseEntity entity.
     *
     * @Route("/{id}/edit", name="caseentity_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, CaseEntity $caseEntity)
    {
        $deleteForm = $this->createDeleteForm($caseEntity);
        $editForm = $this->createForm('AppBundle\Form\CaseEntityType', $caseEntity);
        $editForm->handleRequest($request);

        $this->checkPrividges();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($caseEntity);
            $em->flush();

            return $this->redirectToRoute('caseentity_edit', array('id' => $caseEntity->getId()));
        }

        return $this->render('AppBundle:CaseEntity:edit.html.twig', array(
            'caseEntity' => $caseEntity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a CaseEntity entity.
     *
     * @Route("/{id}", name="caseentity_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, CaseEntity $caseEntity)
    {
        $form = $this->createDeleteForm($caseEntity);
        $form->handleRequest($request);
        $this->checkPrividges();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($caseEntity);
            $em->flush();
        }

        return $this->redirectToRoute('caseentity_index');
    }

    /**
     * Creates a form to delete a CaseEntity entity.
     *
     * @param CaseEntity $caseEntity The CaseEntity entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CaseEntity $caseEntity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('caseentity_delete', array('id' => $caseEntity->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
