<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller {


    public function checkPrividges()
    {
        $user = $this->user();
        
        if ($user == null || !$user->isAdmin()) {
            throw $this->createAccessDeniedException();
        }
    }

    public function user()
    {
        $user = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Person')
            ->findOneByUsername(get_current_user())
        ;

        return $user;
    }

}