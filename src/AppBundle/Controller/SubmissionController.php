<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Submission;
use AppBundle\Form\SubmissionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Submission controller.
 *
 * @Route("/submission")
 */
class SubmissionController extends BaseController
{
    /**
     * Lists all Submission entities.
     *
     * @Route("/", name="submission_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        // $submissions = $em->getRepository('AppBundle:Submission')->findAll();
        $submissions = $this->user()->getSubmissions();

        return $this->render('AppBundle:Submission:index.html.twig', array(
            'submissions' => $submissions,
        ));
    }

    /**
     * Finds and displays a Submission entity.
     *
     * @Route("/{id}", name="submission_show")
     * @Method("GET")
     */
    public function showAction(Submission $submission)
    {
        return $this->render('AppBundle:Submission:show.html.twig', array(
            'submission' => $submission,
        ));
    }

}
