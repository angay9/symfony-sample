<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Person;
use AppBundle\Entity\Submission;
use AppBundle\Form\SubmissionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends BaseController
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $this->createUserIfNotExists();
        $em = $this->getDoctrine()->getManager();
        
        $cases = $em->getRepository('AppBundle:CaseEntity')->findAll();

        $submission = new Submission();

        $form = $this->createCreateForm($submission);

        return $this->render('AppBundle:Default:index.html.twig', [
            'form'  =>  $form->createView(),
            'cases' =>  $cases
        ]);
    }

    /**
     * Creates a new Person entity.
     *
     * @Route("/submission/new", name="submission_new")
     * @Method("POST")
     */
    public function newAction(Request $request)
    {
        $submission = new Submission();
        $em = $this->getDoctrine()->getManager();
        // $this->checkPrividges();

        $form = $this->createCreateForm($submission);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            
            $submission->setReference(
                $this->user()->getId() . date('Ymdhm')
            );

            $submission->setPerson($this->user());
            
            $this->saveAndConvertFiles($submission);
            
            $this->sendEmail($submission);
            $em->persist($submission);
            $em->flush();


            return $this->redirectToRoute('submission_index');
        }

        return $this->render('AppBundle:Default:index.html.twig', array(
            'submission' => $submission,
            'cases' => $em->getRepository('AppBundle:CaseEntity')->findAll(),
            'form' => $form->createView(),
        ));
    }

    private function createCreateForm(Submission $submission)
    {
        $em = $this->getDoctrine()->getManager();
        $cases = $em->getRepository('AppBundle:CaseEntity')->findAll();

        $costCenters = $em->getRepository('AppBundle:CostCenter')->findAll();

        return $this->createForm(SubmissionType::class, $submission, [
            'cases'   =>  array_map(function ($case) {
                return $case->getName();
            }, $cases),
            'costCenters'   =>  array_map(function ($costCenter) {
                return $costCenter->getCostcenter();
            }, $costCenters),
            'action'    =>  $this->generateUrl('submission_new'),
            'em'    =>  $em
        ]);
    }

    private function createUserIfNotExists()
    {
        $username = get_current_user();

        $username = 'Fil';
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:Person')->findOneByUsername($username);

        if ($user) {
            return;
        }

        $user = new Person();
        $user->setUsername($username);
        $user->setEmail($username . '@' . $this->container->getParameter('company_name') . '.com');

        $em->persist($user);
        $em->flush();
    }

    private function sendEmail($submission)
    {
        
        $view = $this->renderView('AppBundle:Email:submission.html.twig', [
            'submission' => $submission
        ]);

        $files = array_filter([
            $submission->getAttachment1(),
            $submission->getAttachment2(),
            $submission->getAttachment3(),
            $submission->getAttachment4(),
            $submission->getAttachment5()
        ]);
        
        $dir = $this->container->getParameter('submissions_dir');
        $message = \Swift_Message::newInstance()
            ->setSubject('New submission')
            ->setFrom('app@example.com')
            ->setTo($this->user()->getEmail())
            ->setBody(
                $view,
                'text/html'
            )
        ;

        foreach ($files as $file) {
            $file = $dir . '/' . $this->user()->getId() . $file;
            if (file_exists($file)) {
                $message->attach(\Swift_Attachment::fromPath($file));
            }
        }
        
        $this->get('mailer')->send($message);

    }

    private function saveAndConvertFiles(& $submission) 
    {
        $files = array_filter([
            'attachment1' => $submission->getAttachment1File(),
            'attachment2' => $submission->getAttachment2File(),
            'attachment3' => $submission->getAttachment3File(),
            'attachment4' => $submission->getAttachment4File(),
            'attachment5' => $submission->getAttachment5File()
        ]);

        $user = $this->user();

        $dir = $this->container->getParameter('submissions_dir') . '/' . $user->getId();
        
        if (!file_exists($dir)) {
            mkdir($dir, 0775, true);
        }

        foreach ($files as $key => $file) {
            $setter = 'set' . ucfirst($key);

            $filename = sha1($file->getClientOriginalName() . $user->getId() . time());
            $ext = $file->getClientOriginalExtension();
            $filename = $filename . '.' . $ext;
            
            $file->move(
                $dir,
                $filename    
            );
            $submission->$setter($filename);
        }
    }

}
